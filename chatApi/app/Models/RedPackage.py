from sqlalchemy_serializer import SerializerMixin
from app.Models.Base import Base
from app.Models.Model import ThRedPackage

class RedPackage(Base, ThRedPackage, SerializerMixin):
    pass