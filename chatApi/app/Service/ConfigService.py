'''
@Author: hua
@Date: 2019-11-04 10:54:31
@description: 
@LastEditors: hua
@LastEditTime: 2019-11-07 15:24:01
'''
import json
import os
from app.Models.Config import Config

from app.Vendor.Utils import Utils


class ConfigService():
    
    '''
    @description: 获取常量配置
    @param: dict
    @return: dict
    '''
    @staticmethod
    def getConst(params:dict)->dict:
        path = os.getcwd()+'/app/const.json'
        with open(path, "rb") as f:
            const = json.loads(f.read(), encoding='utf-8')
        default_img_data = Config().getOne(
            {Config.type == 'img', Config.code == 'default.img', Config.status == 1})
        if default_img_data == None:
            default_img = 'static/img/about/python.jpg'
        else:
            default_img = default_img_data['config']
        const['DEFAULTIMG'] = default_img
        return Utils.formatBody(const)
